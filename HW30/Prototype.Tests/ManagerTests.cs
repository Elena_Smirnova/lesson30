﻿using Prototype.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Tests
{
    [TestFixture]
    public class ManagerTests : Base
    {

        [Test]
        public void TestCloneManager()
        {
            var manager = GetManager();
            var cloneManager = (Manager)manager.Clone();

            Assert.IsNotNull(cloneManager);
            Assert.AreNotSame(manager, cloneManager);
            Assert.AreEqual(manager.Name, cloneManager.Name);
            Assert.AreEqual(manager.Salary, cloneManager.Salary);
            Assert.AreEqual(manager.WorkHours, cloneManager.WorkHours);

        }

        [Test]
        public void TestMyCloneManager()
        {
            var manager = GetManager();
            var cloneManager = manager.MyClone();

            Assert.IsNotNull(cloneManager);
            Assert.AreNotSame(manager, cloneManager);
            Assert.AreEqual(manager.Name, cloneManager.Name);
            Assert.AreEqual(manager.Salary, cloneManager.Salary);
            Assert.AreEqual(manager.WorkHours, cloneManager.WorkHours);

        }

    }
}
