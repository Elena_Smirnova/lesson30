﻿using Prototype.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Tests
{

    public class Base
    {
        public Person GetPerson()
        {
            return new Person("Alex");
        }

        public Employee GetEmployee()
        {
            return new Employee("Bob", 10000);
        }

        public Manager GetManager()
        {
            return new Manager("Lui", 5000, 5);
        }


    }
}
