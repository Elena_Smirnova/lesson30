﻿using Prototype.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Tests
{
    [TestFixture]
    public class EmployeeTests : Base
    {
        [Test]
        public void TestCloneEmployee()
        {
            var employee = GetEmployee();
            var cloneEmployee = (Employee)employee.Clone();

            Assert.IsNotNull(cloneEmployee);
            Assert.AreNotSame(employee, cloneEmployee);
            Assert.AreEqual(employee.Name, cloneEmployee.Name);
            Assert.AreEqual(employee.Salary, cloneEmployee.Salary);

        }


        [Test]
        public void TestMyCloneEmployee()
        {
            var employee = GetEmployee();
            var cloneEmployee = employee.MyClone();

            Assert.IsNotNull(cloneEmployee);
            Assert.AreNotSame(employee, cloneEmployee);
            Assert.AreEqual(employee.Name, cloneEmployee.Name);
            Assert.AreEqual(employee.Salary, cloneEmployee.Salary);

        }




    }
}
