﻿using Prototype.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Tests
{
    [TestFixture]
    public class PersonTests : Base
    {


        [Test]
        public void TestClonePerson()
        {
            var person = GetPerson();
            var clonePerson = (Person)person.Clone();

            Assert.IsNotNull(clonePerson);
            Assert.AreNotSame(person, clonePerson);
            Assert.AreEqual(person.Name, clonePerson.Name);

        }

        [Test]
        public void TestMyClonePerson()
        {
            var person = GetPerson();
            var clonePerson = person.MyClone();

            Assert.IsNotNull(clonePerson);
            Assert.AreNotSame(person, clonePerson);
            Assert.AreEqual(person.Name, clonePerson.Name);

        }


    }
}
