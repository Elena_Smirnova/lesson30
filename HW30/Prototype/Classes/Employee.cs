﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Classes
{
    public class Employee : Person
    {
        public double Salary;

        public Employee(string name,double salary): base(name)
        {
            Salary = salary;   
        }



        public override Employee MyClone()
        {
            return new Employee(Name,Salary);
        }

        public override object Clone()
        {
            return MyClone();
        }


    }
}
