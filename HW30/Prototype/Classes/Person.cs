﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Classes
{
    public class Person : ICloneable, IMyCloneable<Person>
    {
        public string Name;

        public Person(string name)
        {
            Name = name;
        }


        public virtual Person MyClone()
        {
            return new Person(Name);
        }

        public virtual object Clone()
        {
            return MyClone();
        }

    }
}
