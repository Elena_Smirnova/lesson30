﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.Classes
{
    public class Manager : Employee
    {
        public int WorkHours;

        public Manager(string name, double salary,int workHours):base(name,salary)
        {
            WorkHours = workHours;
        }




        public override Manager MyClone()
        {
            return new Manager(Name,Salary,WorkHours);  
        }


        public override object Clone()
        {
            return MyClone();
        }

    }
}
